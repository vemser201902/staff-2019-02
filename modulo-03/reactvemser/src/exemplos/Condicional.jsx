import React from 'react';

const Dropdown = props => {
    return (
        <React.Fragment>
            <div className="dropdown">
                <button className="dropbtn">Gêneros</button>
                <div className="dropdown-content" id="div-drop">
                    { props.lista.map((teste) => { 
                        return (<div>{teste.nome}</div>)
                     }) }
                </div>
            </div>
        </React.Fragment>
    )
} 
export default Dropdown;