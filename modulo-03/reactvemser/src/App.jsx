import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/ListaEpisodios';
//import EpisodioPadrao from './components/EpisodioPadrao';
//import TesteRenderizacao from './components/TesteRenderizacao'
import Condicional from './exemplos/Condicional';

class App extends Component {
  constructor ( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.listaEpisodios.seriesInvalidas()
    this.setState({
      episodio
    })
  }

  marcarComoAssistido() {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio
    } )
  }

  registrarNota( evt ) {
    const { episodio } = this.state
    episodio.avaliar( evt.target.value )
    this.setState({
      episodio,
      exibirMensagem: true
    })
    setTimeout( () => {
      this.setState( {
        exibirMensagem: false
      } )
    }, 5000)
  }

  geraCampoDeNota() {
    // https://reactjs.org/docs/conditional-rendering.html
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para este episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) }></input>
            </div>
          )
        }
      </div>
    )
  }

  render() {
    const { episodio, exibirMensagem } = this.state
    return (
      <div className="App">
        <div className="App-Header">
          {/* <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear.bind( this ) } MarcarNoComp={ this.marcarComoAssistido.bind( this ) } />
          { this.geraCampoDeNota() }
          <h5>{ exibirMensagem ? 'Nota Registrada com sucesso!' : '' }</h5> */}
          {/* <TesteRenderizacao nome={ 'Marcos' } >
            <h4>Aqui novamente</h4>
          </TesteRenderizacao> */}
          <Condicional lista={ this.listaEpisodios } metodosSortear={ this.sortear.bind( this )} />
        </div>
      </div>
    );
  }
}

export default App;
