let pokeApi = new PokeApi();

/* let pokemonEspecifico = pokeApi.buscarEspecifico(12);
pokemonEspecifico.then( pokemon => {
    let poke = new Pokemon( pokemon );
    renderizacaoPokemon( poke );
}) */

async function buscar() {
    let pokemonEspecifico = await pokeApi.buscarEspecifico(12);
    let poke = new Pokemon( pokemonEspecifico );
    renderizacaoPokemon( poke );
}
buscar();

function renderizacaoPokemon( pokemon ) {
    let dadosPokemon = document.getElementById('dadosPokemon');
    let nome = dadosPokemon.querySelector('.nome');
    nome.innerHTML = pokemon.nome;
}