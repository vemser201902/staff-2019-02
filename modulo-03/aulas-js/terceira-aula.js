function cardapioIFood( veggie = true, comLactose = false ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]
    
    if ( comLactose ) {
      cardapio.push( 'pastel de queijo' )
    }
    
    cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa'];

    /* cardapio = cardapio.concat( [
      'pastel de carne',
      'empada de legumes marabijosa'
    ] ) */
  
    if ( veggie ) {
      // TODO: remover alimentos com carne (é obrigatório usar splice!)
      cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1 )
      cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
    }
    
    let resultado = cardapio
                        //.filter(alimento => alimento === 'cuca de uva')
                        .map(alimento => alimento.toUpperCase());
    return resultado;
  }
  
  //console.log(cardapioIFood(true, true)) // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]


  function criarSanduiche(pao, recheio, queijo) {
      console.log(`Seu Sanduiche tem o pão ${pao} com recheio de ${recheio} e queijo ${ queijo } `);
  }

  const ingredientes = ['3 queijos', 'Frango', 'cheddar']
  //criarSanduiche(...ingredientes);

  function receberValoresIndefinidos(...valores) {
      valores.map(valor => console.log(valor));
  }

  //receberValoresIndefinidos([1, 3, 4, 5]);
  //console.log({... "Marcos"});

  /* let inputTeste = document.getElementById('campoTeste')
  inputTeste.addEventListener('blur', () => cardapioIFood(true, true)); */

  /* let objetoTeste = {
    nome: "Marcos"
  }

  console.log(objetoTeste); */

  /* String.prototype.correr = function(upper = false){
    let texto = `${this} estou correndo`;
    return upper ? texto.toUpperCase() : texto;
  } */
  let functCorrer = function(upper = false){
    let texto = `${this} estou correndo`;
    return upper ? texto.toUpperCase() : texto;
  }

  String.prototype.correr = functCorrer
  Boolean.prototype.correr = functCorrer
  //Object.prototype.correr = functCorrer
  
  console.log("eu".correr());
  console.log(true.correr());
