import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './info.css';
import ListaSeries from '../models/ListaSeries';

export default class InfoJsFlix extends Component {
  constructor ( props ) {
    super( props )
    this.listaSeries = new ListaSeries();
    this.state = {
      series: this.listaSeries.todos,
      exibirMensagem: false,
      conteudo: ''
    }
  }

  invalidas(){
    const invalidas = this.listaSeries.invalidas();
    this.setState({
      conteudo: invalidas
    })
  }

  palavraChave(){
    const palavra = this.listaSeries.mostrarPalavraSecreta();
    this.setState({
      conteudo: palavra
    })
  }
  
  render(){
    return (
      <div className="App">
        <Menu />
        <header className="App-header">
          <nav>
            <ul>
              <li><button onClick={ this.invalidas.bind( this ) }>Series Inválidas</button></li>
              <li><button onClick={ this.palavraChave.bind( this ) }>Filtrar por ano</button></li>
              <li><button onClick={ this.palavraChave.bind( this ) }>Procurar por nome</button></li>
              <li><button onClick={ this.palavraChave.bind( this ) }>Média</button></li>
              <li><button onClick={ this.palavraChave.bind( this ) }>Total Salario</button></li>
              <li><button onClick={ this.palavraChave.bind( this ) }>Generos</button></li>
              <li><button onClick={ this.palavraChave.bind( this ) }>Titulos</button></li>
              <li><button onClick={ this.palavraChave.bind( this ) }>Créditos</button></li>
              <li><button onClick={ this.palavraChave.bind( this ) }>Palavra Secreta</button></li>
            </ul>
          </nav>
          
          <div style={{color: '#fff'}}>{ this.state.conteudo }</div>
        </header>
      </div>
    );
  }
}

const Menu = () => 
  <section className="principal">
    <Link to="/" >Pagina Inicial</Link>
    <Link to="/infoFlix">Info JsFlix</Link>
  </section>