import React from 'react'
import BotaoDoSistema from './BotaoDoSistema';

const EpisodioPadrao = props => {
    const { episodio } = props
    return (
        <React.Fragment>
            <div className="botoes">
                <BotaoDoSistema cor="verde" quandoClicar={ props.sortearNoComp } texto="Próximo" />
                <BotaoDoSistema cor="azul" quandoClicar={ props.MarcarNoComp } texto="Já assisti" />
            </div>
            <h2>{ episodio.nome }</h2>
            <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
            <h4>{ episodio.duracaoEmMin }</h4>
            <h4>Temp/Ep: { episodio.temporadaEpisodio }</h4>
            <h4>Já Assisti? { episodio.assistido ? 'Sim' : 'Não' }, {episodio.qtdVezesAssistido} vez(es)</h4>
            <h4>{ episodio.nota || 'Sem Nota'}</h4>
        </React.Fragment>
    )
}

export default EpisodioPadrao