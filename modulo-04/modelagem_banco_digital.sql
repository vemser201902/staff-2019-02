<?xml version="1.0" encoding="utf-8" ?>
<!-- SQL XML created by WWW SQL Designer, https://github.com/ondras/wwwsqldesigner/ -->
<!-- Active URL: https://ondras.zarovi.cz/sql/demo/?keyword=default -->
<sql>
<datatypes db="pgsql">
	<group label="More Usual" color="rgb(138,138,270)">
		<type label="Serial" length="0" sql="serial" re="seriali4" quote=""/>
		<type label="Big Integer" length="0" sql="bigint" re="int8" quote=""/>
		<type label="Text" length="0" sql="text" quote="'"/>
	</group>
	<group label="Numeric" color="rgb(238,238,170)">
		<type label="Big Serial" length="0" sql="bigserial" re="serial8" quote=""/>
		<type label="Small Integer" length="0" sql="smallint" re="int2" quote=""/>
		<type label="Integer" length="0" sql="integer" re="int4" quote=""/>
		<type label="Numeric" length="1" sql="numeric" re="decimal" quote=""/>
		<type label="Real" length="0" sql="real" re="float4" quote=""/>
		<type label="Double Precision" length="0" sql="double precision" re="float8" quote=""/>
	</group>

	<group label="Character" color="rgb(255,200,200)">
		<type label="Character" length="1" sql="character" re="char" quote="'"/>
		<type label="Character Varying" length="1" sql="character varying" re="varchar" quote="'"/>
		<type label="XML" length="0" sql="xml" quote="'"/>
	</group>
	
	<group label="Date &amp; Time" color="rgb(200,255,200)">
		<type label="Timestamp" length="0" sql="timestamp" quote="'"/>
		<type label="Timestamp With Timezone" length="0" sql="timestamp with timezone" re="timestamptz" quote="'"/>
		<type label="Interval" length="0" sql="interval" quote="'"/>
		<type label="Date" length="0" sql="date" quote="'"/>
		<type label="Time" length="0" sql="time" quote="'"/>
		<type label="Time With Timezone" length="0" sql="time with timezone" re="timetz" quote="'"/>
	</group>
        
	<!-- 
	<group label="Geometric" color="rgb(100,155,100)">
	FIXME 
	</group>
        -->

        <!--
        <group label="Network Address" color="rgb(100,155,100)">
        FIXME
        </group>
        -->

        <!--
        <group label="Bit String" color="rgb(100,155,100)">
        FIXME
        </group>
        -->

        <!--
        <group label="Text Search" color="rgb(100,155,100)">
        FIXME
        </group>
        -->

        <!--
        <group label="Arrays" color="rgb(100,155,100)">
        FIXME
        </group>a
        -->

	<group label="Miscellaneous" color="rgb(200,200,255)">
		<type label="Binary" length="0" sql="bytea" quote="'"/>
		<type label="Boolean" length="0" sql="boolean" re="boll" quote="'"/>
		<type label="UUID" length="0" sql="uuid" quote="'"/>
		<type label="Enum" length="1" sql="enum" quote="'"/>
		<type label="Money" length="1" sql="money" quote=""/>
	</group>
</datatypes><table x="215" y="35" name="BANCOS">
<row name="ID_BANCO" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="CODIGO" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>0</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>character varying(100)</datatype>
<default>'NULL'</default></row>
<key type="PRIMARY" name="">
<part>ID_BANCO</part>
</key>
</table>
<table x="426" y="163" name="AGENCIAS">
<row name="ID_AGENCIA" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="FK_ID_BANCO" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="BANCOS" row="ID_BANCO" />
</row>
<row name="CODIGO" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>0</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>character varying(100)</datatype>
<default>'NULL'</default></row>
<row name="ID_ENDERECO_ENDERECOS" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="ENDERECOS" row="ID_ENDERECO" />
</row>
<key type="PRIMARY" name="">
<part>ID_AGENCIA</part>
</key>
</table>
<table x="680" y="167" name="ENDERECOS">
<row name="ID_ENDERECO" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="LOGRADOURO" null="0" autoincrement="0">
<datatype>character varying(255)</datatype>
<default>'NULL'</default></row>
<row name="NUMERO" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>0</default></row>
<row name="COMPLEMENTO" null="1" autoincrement="0">
<datatype>character varying(100)</datatype>
<default>NULL</default></row>
<row name="FK_ID_BAIRRO" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="BAIRROS" row="ID_BAIRROS" />
</row>
<key type="PRIMARY" name="">
<part>ID_ENDERECO</part>
</key>
</table>
<table x="956" y="138" name="BAIRROS">
<row name="ID_BAIRROS" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>character varying(100)</datatype>
<default>'NULL'</default></row>
<row name="FK_ID_CIDADE" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="CIDADES" row="ID_CIDADE" />
</row>
<key type="PRIMARY" name="">
<part>ID_BAIRROS</part>
</key>
</table>
<table x="301" y="510" name="CORRENTISTAS">
<row name="ID_CORRENTISTA" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="RAZAO_SOCIAL" null="1" autoincrement="0">
<datatype>character varying</datatype>
<default>NULL</default></row>
<row name="CNPJ" null="1" autoincrement="0">
<datatype>character(14)</datatype>
<default>NULL</default></row>
<row name="TIPO" null="0" autoincrement="0">
<datatype>enum</datatype>
<default>'NULL'</default><comment>PJ, PF, CONJUNTA, INVESTIMENTO</comment>
</row>
<key type="PRIMARY" name="">
<part>ID_CORRENTISTA</part>
</key>
</table>
<table x="81" y="300" name="AGENCIAS_X_CORRENTISTAS">
<row name="ID" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="FK_ID_AGENCIA" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="AGENCIAS" row="ID_AGENCIA" />
</row>
<row name="FK_ID_CORRENTISTA" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="CORRENTISTAS" row="ID_CORRENTISTA" />
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="1185" y="92" name="CIDADES">
<row name="ID_CIDADE" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>character varying(100)</datatype>
<default>'NULL'</default></row>
<row name="FK_ID_ESTADO" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="ESTADOS" row="ID_ESTADO" />
</row>
<key type="PRIMARY" name="">
<part>ID_CIDADE</part>
</key>
</table>
<table x="1426" y="58" name="ESTADOS">
<row name="ID_ESTADO" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>character varying(100)</datatype>
<default>'NULL'</default></row>
<row name="FK_ID_PAIS" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="PAISES" row="ID_PAIS" />
</row>
<key type="PRIMARY" name="">
<part>ID_ESTADO</part>
</key>
</table>
<table x="1673" y="40" name="PAISES">
<row name="ID_PAIS" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>character varying</datatype>
<default>'NULL'</default></row>
<key type="PRIMARY" name="">
<part>ID_PAIS</part>
</key>
</table>
<table x="703.6666679382324" y="421" name="CLIENTES">
<row name="ID_CLIENTE" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="CPF" null="0" autoincrement="0">
<datatype>character(11)</datatype>
<default>'NULL'</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>character varying(100)</datatype>
<default>'NULL'</default></row>
<row name="RG" null="0" autoincrement="0">
<datatype>character varying(15)</datatype>
<default>'NULL'</default></row>
<row name="CONJUGE" null="1" autoincrement="0">
<datatype>character varying</datatype>
<default>NULL</default></row>
<row name="DATA_NASCIMENTO" null="0" autoincrement="0">
<datatype>character(10)</datatype>
<default>'NULL'</default></row>
<row name="ESTADO_CIVIL" null="0" autoincrement="0">
<datatype>enum</datatype>
<default>'NULL'</default><comment>SOLTEIRO, CASADO, DIVORCIADO, VIUVO</comment>
</row>
<row name="ID_ENDERECO_ENDERECOS" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="ENDERECOS" row="ID_ENDERECO" />
</row>
<key type="PRIMARY" name="">
<part>ID_CLIENTE</part>
</key>
</table>
<table x="1453.6666564941406" y="296.3333435058594" name="TELEFONES">
<row name="ID_TELEFONE" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="NUMERO" null="0" autoincrement="0">
<datatype>character varying(20)</datatype>
<default>'NULL'</default></row>
<row name="TIPO" null="0" autoincrement="0">
<datatype>enum</datatype>
<default>'NULL'</default><comment>FIXO, CELULAR</comment>
</row>
<key type="PRIMARY" name="">
<part>ID_TELEFONE</part>
</key>
</table>
<table x="1027" y="306" name="CLIENTES_X_TELEFONES">
<row name="ID" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="ID_CLIENTE_CLIENTES" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="CLIENTES" row="ID_CLIENTE" />
</row>
<row name="ID_TELEFONE_TELEFONES" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="TELEFONES" row="ID_TELEFONE" />
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="1108" y="710" name="EMAILS">
<row name="ID_EMAIL" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="VALOR" null="0" autoincrement="0">
<datatype>character varying(100)</datatype>
<default>'NULL'</default></row>
<row name="ID_CLIENTE_CLIENTES" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="CLIENTES" row="ID_CLIENTE" />
</row>
<key type="PRIMARY" name="">
<part>ID_EMAIL</part>
</key>
</table>
<table x="433.3333339691162" y="702" name="CORRENTISTAS_X_CLIENTES">
<row name="ID" null="0" autoincrement="1">
<datatype>integer</datatype>
<default>NULL</default></row>
<row name="ID_CORRENTISTA_CORRENTISTAS" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="CORRENTISTAS" row="ID_CORRENTISTA" />
</row>
<row name="ID_CLIENTE_CLIENTES" null="0" autoincrement="0">
<datatype>integer</datatype>
<default>NULL</default><relation table="CLIENTES" row="ID_CLIENTE" />
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
</sql>




I
-- INSERT INTO PAISES(ID_PAIS, NOME) VALUES(PAISES_SEQ.NEXTVAL, 'BRASIL')
-- INSERT INTO PAISES(ID_PAIS, NOME) VALUES(PAISES_SEQ.NEXTVAL, 'ARGENTINA')
-- INSERT INTO PAISES(ID_PAIS, NOME) VALUES(1, 'CHILE')

-- SELECT * FROM PAISES WHERE ID_PAIS = 1
-- SELECT * FROM PAISES ORDER BY ID_PAIS DESC

-- UPDATE PAISES SET NOME = 'ARGENTINA 2' WHERE ID_PAIS = 1
-- DELETE FROM PAISES WHERE ID_PAIS = 1

-- SELECT * FROM ESTADOS

-- INSERT INTO ESTADOS(ID_ESTADO, NOME, FK_ID_PAIS) VALUES(ESTADOS_SEQ.NEXTVAL, 'PARÁ', 3)
/*INSERT INTO ESTADOS(ID_ESTADO, NOME, FK_ID_PAIS) VALUES(ESTADOS_SEQ.NEXTVAL, 'BAHIA',
    (SELECT ID_PAIS FROM PAISES WHERE NOME = 'BRASIL')
)*/


-- SELECT EST.ID_ESTADO, EST.NOME, P.NOME PAIS FROM ESTADOS EST INNER JOIN PAISES P ON EST.FK_ID_PAIS = P.ID_PAIS
-- SELECT EST.ID_ESTADO, EST.NOME, P.NOME PAIS FROM ESTADOS EST RIGHT JOIN PAISES P ON EST.FK_ID_PAIS = P.ID_PAIS
-- SELECT EST.ID_ESTADO, EST.NOME, P.NOME PAIS FROM PAISES P LEFT JOIN ESTADOS EST ON EST.FK_ID_PAIS = P.ID_PAIS

-- TRUNCATE TABLE PAISES;

COMMIT
ROLLBACK





