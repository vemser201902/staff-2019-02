package Entity;

import javax.persistence.*;

@Entity
@SequenceGenerator(allocationSize = 1, name = "ESTADOS_SEQ",
        sequenceName = "ESTADOS_SEQ")
public class Estados {

    @Id
    @GeneratedValue(generator = "ESTADOS_SEQ",
            strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
