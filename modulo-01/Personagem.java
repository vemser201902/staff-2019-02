public abstract class Personagem{
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida, qtdDano;
    protected int experiencia,qtdExperienciaPorAtaque;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario(0);
        this.experiencia = 0;
        this.qtdDano = 0.0;
        this.qtdExperienciaPorAtaque = 1;
    }
    
    protected Personagem(String nome){
        this.nome = nome;
    }
    
    protected String getNome(){
        return this.nome;
    }
    
    protected void setNome(String nome){
        this.nome = nome;
    }
    
    protected Inventario getInventario(){
        return this.inventario;
    }
    
    protected double getVida(){
        return this.vida;
    }
    
    protected Status getStatus(){
        return this.status;
    }
    
    protected int getExperiencia(){
        return this.experiencia;
    }
    
    protected void ganharItem(Item item){
        this.inventario.adicionar(item);
    }
    
    protected void perderItem(Item item){
        this.inventario.remover(item);
    }
    
    protected void aumentarXp(){
        experiencia = experiencia + this.qtdExperienciaPorAtaque;
    }

    protected boolean podeSofrerDano(){
        return this.vida > 0;
    }
    
    protected double calcularDano(){
        return this.qtdDano;
    }
    
    protected void sofreDano(){
        this.vida -= this.vida >= this.calcularDano() ? this.calcularDano() : this.vida;
        if(this.vida == 0.0){
            this.status = Status.MORTO;
        }else{
            this.status = Status.SOFREU_DANO;
        }
    }

    protected abstract String imprimirResultado();

    
    
    
    
    
    
    
    
    
}
