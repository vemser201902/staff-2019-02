import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest{
    @Test
    public void adicionarUmItem(){
        Inventario inventario = new Inventario(4);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.getItens().get(0));
    }
    
    @Test
    public void adicionarDoisItem(){
        Inventario inventario = new Inventario(4);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
    }
    
    /*@Test
    public void adicionarDoisItensComEspacoParaUmNaoAdicionaSegundo(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(1, inventario.getItens().size() );
    }*/
    
    @Test
    public void obterItemNaPrimeiraPosicao(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.obter(0));
    }
    
    @Test
    public void obterItemNaoAdicionado(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        assertNull(inventario.obter(0));
    }
    
    @Test
    public void removerItem(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.remover(espada);
        assertNull(inventario.obter(0));
    }
    
    @Test
    public void removerItemAntesDeAdicionarProximo(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(espada);
        inventario.remover(espada);
        inventario.adicionar(escudo);
        assertEquals(escudo, inventario.obter(0));
        assertEquals(1, inventario.getItens().size());
    }
    
    @Test
    public void getDescricoesVariosItens(){
        Inventario inventario = new Inventario(4);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(armadura);
        assertEquals("Espada,Escudo,Armadura", inventario.getDescricoesItens());
    }
    
    @Test
    public void getDescricoesNenhumItem(){
        Inventario inventario = new Inventario(4);
        assertEquals("", inventario.getDescricoesItens());
    }
    
    @Test
    public void getItemMaiorQuantidadeComVarios(){
        Inventario inventario = new Inventario(4);
        Item lanca = new Item(1, "Lança");
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(3, "Escudo");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(escudo, inventario.getItemComMaiorQuantidade());
    }
    
    @Test
    public void getItemMaiorQuantidadeInventarioVazio(){
        Inventario inventario = new Inventario(0);
        assertNull(inventario.getItemComMaiorQuantidade());
    }
    
    @Test
    public void getItemMaiorQuantidadeComItensComMesmaQuantidade(){
        Inventario inventario = new Inventario(4);
        Item lanca = new Item(1, "Lança");
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(lanca, inventario.getItemComMaiorQuantidade());
    }
    
    @Test
    public void buscarItemComInventarioVazio(){
        Inventario inventario = new Inventario(0);
        assertNull(inventario.buscar("Capa"));
    }
    
    @Test
    public void buscarApenasUmItem(){
        Inventario inventario = new Inventario(1);
        Item termica = new Item(1, "Térmica de café");
        inventario.adicionar(termica);
        assertEquals(termica, inventario.buscar("Térmica de café"));
    }
    
    @Test
    public void buscarApenasUmItemComMesmaDescricao(){
        Inventario inventario = new Inventario(1);
        Item termica1 = new Item(1, "Térmica de café");
        Item termica2 = new Item(2, "Térmica de café");
        inventario.adicionar(termica1);
        inventario.adicionar(termica2);
        assertEquals(termica1, inventario.buscar("Térmica de café"));
    }
    
    @Test
    public void inverterInventarioVazio(){
        Inventario inventario = new Inventario(0);
        assertTrue(inventario.inverter().isEmpty());
    }
    
    @Test
    public void inverterComApenasUmItem(){
        Inventario inventario = new Inventario(1);
        Item termica = new Item(1, "Térmica de café");
        inventario.adicionar(termica);
        assertEquals(termica, inventario.inverter().get(0));
        assertEquals(1, inventario.inverter().size());
    }
    
    @Test
    public void inverterComDoisItens(){
        Inventario inventario = new Inventario(2);
        Item termica = new Item(1, "Térmica de café");
        Item caneca = new Item(2, "Caneca");
        inventario.adicionar(termica);
        inventario.adicionar(caneca);
        assertEquals(caneca, inventario.inverter().get(0));
        assertEquals(2, inventario.inverter().size());
    }
    
    
    
    
    
    
}
