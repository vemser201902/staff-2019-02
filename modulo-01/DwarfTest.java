import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    
    private final double DELTA = 1e-9;
    
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        assertEquals(110.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfPerde10DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        dwarf.sofreDano();
        assertEquals(100.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfPerdeTodaVida11Ataques(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        
        for(int i = 0; i < 11; i++){
            dwarf.sofreDano();
        }
        
        assertEquals(0.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNasceComStatus(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfPerdeVidaEContinuaVivo(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.sofreDano();
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfPerdeVidaEDeveMorrer(){
        Dwarf dwarf = new Dwarf("Gimli");
        for(int i = 0; i < 12; i++){
            dwarf.sofreDano();
        }
        assertEquals(Status.MORTO, dwarf.getStatus());
        assertEquals(0.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals("Escudo", dwarf.getInventario().buscar("Escudo").getDescricao());
    }
    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDano(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.equiparEscudo();
        dwarf.sofreDano();
        assertEquals(105.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.sofreDano();
        assertEquals(100.0, dwarf.getVida(), DELTA);
    }
    
    
    
    
    
    
    
    
    
}
