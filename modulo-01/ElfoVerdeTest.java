import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    
    @Test
    public void elfoVerdeGanha2XpPorUmaFlecha() {
        ElfoVerde celebron = new ElfoVerde("Celebron");
        celebron.atirarFlecha(new Dwarf("Balin"));
        assertEquals(2, celebron.getExperiencia());
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoValida() {
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celebron.ganharItem(arcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
        assertEquals(arcoDeVidro, inventario.obter(2));
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoInvalida() {
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeMadeira = new Item(1, "Arco de Madeira");
        celebron.ganharItem(arcoDeMadeira);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de Madeira"));
    }
    
    @Test
    public void elfoVerdePerdeItemComDescricaoValida() {
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celebron.ganharItem(arcoDeVidro);
        celebron.perderItem(arcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de Vidro"));
    }

    @Test
    public void elfoVerdePerdeItemComDescricaoInvalida() {
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arco = new Item(1, "Arco");
        celebron.perderItem(arco);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
    }
    
    
    
    
    
    
    
    
}