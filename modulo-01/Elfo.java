public class Elfo extends Personagem{
    private int indiceFlecha;
    private static int qtdElfos;

    {
        this.inventario = new Inventario(2);
        this.indiceFlecha = 1;
    }
    
    public Elfo(String nome){
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
        Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos() {
        return Elfo.qtdElfos;
    }
        
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
    public boolean podeAtirarFlecha(){
        return this.getFlecha().getQuantidade() > 0;
    }
    
    public void atirarFlecha(Dwarf dwarf){
        int qtdAtual = this.getFlecha().getQuantidade();
        
        if(this.podeSofrerDano() && podeAtirarFlecha()){
            this.getFlecha().setQuantidade(qtdAtual - 1);
            this.sofreDano();
            dwarf.sofreDano();
            this.aumentarXp();
        }       
    }
    
    public String imprimirResultado() {
        return "Elfo";
    }
    
    
    
    
    
    
}
