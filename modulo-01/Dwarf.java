public class Dwarf extends Personagem{
    private boolean equipado = false;
    
    public Dwarf(String nome){
        super(nome);
        this.vida = 110.0;
        this.qtdDano = 10.0;
        this.inventario.adicionar(new Item(1, "Escudo"));
    }
    
    public void equiparEscudo(){
        this.equipado = true;
    }
    
    public double calcularDano(){
        return this.equipado ? 5.0 : this.qtdDano;
    }
    
    public String imprimirResultado() {
        return "Dwarf";
    }
    
    
}
